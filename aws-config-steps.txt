1) Install Powershell
    -   https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-7

        # Download the Microsoft repository GPG keys
        wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb

        # Register the Microsoft repository GPG keys
        sudo dpkg -i packages-microsoft-prod.deb

        # Update the list of products
        sudo apt-get update

        # Enable the "universe" repositories
        sudo add-apt-repository universe

        # Install PowerShell
        sudo apt-get install -y powershell

        # Start PowerShell
        pwsh

    -   (delete .deb package: `rm packages-microsoft-prod.deb`)
2) Install Octopus Tentacle
    -   https://octopus.com/docs/infrastructure/deployment-targets/linux/tentacle

        sudo apt-key adv --fetch-keys https://apt.octopus.com/public.key
        sudo add-apt-repository "deb https://apt.octopus.com/ stretch main"
        sudo apt-get update
        sudo apt-get install tentacle
3) Install Docker via Octopus
4) Install Docker Compose via Octopus
5) Install Package 


