using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
                    .AddRazorViewEngine();
        }

        public void Configure(IApplicationBuilder app)
        {
            var env = app.ApplicationServices.GetService<IWebHostEnvironment>();
            var config = app.ApplicationServices.GetService<IRuntimeConfig>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions{
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.Use(async (context, next) => {
                if (context.IsServedByClient()){
                    context.Request.Path = new PathString("/app/index.html");
                }

                await next();
            });

            app.UseStaticFiles(new StaticFileOptions{
                FileProvider = new PhysicalFileProvider(config.StaticFilesLocation),
                RequestPath = new PathString("/app")
            });

            app.UseRouting();
            app.UseEndpoints(e => e.MapControllers());
        }
    }
}
