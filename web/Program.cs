using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace web
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var executingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var staticFiles = Path.Combine(executingDirectory, "static");
            if (args.Length > 0) {
                staticFiles = args[0];
            }

            CreateHostBuilder(args, staticFiles).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args, string staticFiles) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(services => services.AddSingleton<IRuntimeConfig>(new RuntimeConfig(staticFiles)))
                .ConfigureWebHostDefaults(webBuilder =>
                    webBuilder.UseStartup<Startup>()
                );
    }
}
