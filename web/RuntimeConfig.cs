namespace web
{

    public class RuntimeConfig : IRuntimeConfig
    {
        public RuntimeConfig(string staticFilesLocation)
        {
            this.StaticFilesLocation = staticFilesLocation;
        }

        public string StaticFilesLocation { get; }
    }
}