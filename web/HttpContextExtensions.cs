using Microsoft.AspNetCore.Http;

namespace web
{
    public static class HttpContextExtensions
    {
        public static bool IsIndex(this HttpContext context){
            return context.Request.Path.Value == "/";
        }

        public static bool IsStaticContent(this HttpContext context) {
            return context.Request.Path.Value.StartsWith("/app");
        }

        public static bool IsApiCall(this HttpContext context){
            return context.Request.Path.Value.StartsWith("/api");
        }

        public static bool IsUnknown(this HttpContext context) {
            return !context.IsStaticContent() && !context.IsApiCall();
        }

        public static bool IsServedByClient(this HttpContext context){
            return context.IsIndex() || context.IsUnknown();
        }

        public static bool IsServedByServer(this HttpContext context){
            return context.IsApiCall() || context.IsStaticContent();
        }
    }
}