namespace web
{
    public interface IRuntimeConfig
    {
        string StaticFilesLocation { get; }
    }
}