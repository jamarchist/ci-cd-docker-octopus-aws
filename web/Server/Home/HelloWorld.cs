using Microsoft.AspNetCore.Mvc;

namespace web.Server.Home
{
    public class HomeController : Controller {

        [Route("/api/hello-world-message")]
        public IActionResult HelloWorld(){
            return Ok(new { Message = "Hello World from an SPA!" });
        }
    }
}