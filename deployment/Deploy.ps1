## This is the long-winded way that I would use to do this if I were publishing build information ##
# $AllPackages = $Octopus.Release.Package
# $CurrentPackage = $AllPackages['hello-world']
# $CurrentVersion = $CurrentPackage.Version

$CurrentVersion = [System.IO.File]::ReadAllLines( ( Resolve-Path ./package-version.txt) )

Write-Output "hello from package $CurrentVersion"