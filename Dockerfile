FROM    node AS client-build
WORKDIR /src
COPY    ./client ./
RUN     npm install
RUN     npm run build

FROM    mcr.microsoft.com/dotnet/core/sdk:3.1 AS server-build
WORKDIR /src
COPY    . ./
RUN     dotnet restore
RUN     dotnet publish -c Release -o /output

FROM    mcr.microsoft.com/dotnet/core/aspnet:3.1
EXPOSE  80
WORKDIR /
COPY    --from=server-build /output /var/www
COPY    --from=client-build /src/dist /var/www/static
ENTRYPOINT ["dotnet", "/var/www/web.dll"]